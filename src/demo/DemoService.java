package demo;

import functional_2.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DemoService {

    public void execute () {

        List <String> strList = new ArrayList<>(Arrays.asList("hello", "howz", "are", "youz"));
        System.out.printf("Дан массив %s удалить все строки, содержащие символ 'z': %s%n",
                strList.toString(),
                new noZ().execute(strList).toString());

        strList = new ArrayList<>(Arrays.asList("aaaa", "bbb", "***", "333"));
        System.out.printf("Дан массив %s удалить все строки, длинее 4 символов включительно: %s%n",
                strList.toString(),
                new noLong().execute(strList).toString());

        List <Integer> intList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7));
        System.out.printf("Дан массив %s возвести все элементы в квадрат и прибавить 10, вернуть получившиеся елементы, " +
                        "которые не заканчиваются на 5 или 6: %s%n",
                intList.toString(),
                new square56().execute(intList).toString());
    }

}
