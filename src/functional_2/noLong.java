/*
Given a list of strings, return a list of the strings, omitting any string length 4 or more.

https://codingbat.com/prob/p194496
 */

package functional_2;

import java.util.List;

public class noLong {

    public List<String> execute(List<String> strings) {

        strings.removeIf(e -> e.length() >= 4);

        return strings;
    }

}
