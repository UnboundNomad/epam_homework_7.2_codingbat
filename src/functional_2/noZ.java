/*
Given a list of strings, return a list of the strings, omitting any string that contains a "z". (Note: the str.contains(x) method returns a boolean)

https://codingbat.com/prob/p105671
 */

package functional_2;

import java.util.List;
import java.util.stream.Collectors;

public class noZ {

    public List<String> execute (List<String> strings) {

        return strings.stream()
                .filter(e -> !e.contains("z"))
                .collect(Collectors.toList());

    }

}
