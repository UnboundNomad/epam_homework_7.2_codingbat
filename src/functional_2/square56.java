/*
Given a list of integers, return a list of those numbers squared and the product added to 10, omitting any of the resulting numbers that end in 5 or 6.

https://codingbat.com/prob/p132748
 */

package functional_2;

import java.util.List;
import java.util.stream.Collectors;

public class square56 {

    public List<Integer> execute(List<Integer> nums) {

        return nums.stream()
                .map(e -> e * e + 10)
                .filter(e -> !(e % 10 == 5 || e % 10 == 6))
                .collect(Collectors.toList());

    }


}
